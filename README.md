**Random Quotation Generator**

This Python script generates random quotations from different categories such as Sad, Happy, Motivational, Love, and Funny. It allows users to select a category and display a random quotation from that category.
**Features**
Displays random quotations from various categories.
Categories include Sad, Happy, Motivational, Love, and Funny.
Uses SQLite database to store and retrieve quotations.
Implemented using Tkinter for the graphical user interface (GUI).
Dependencies
Python 3.x
Tkinter (usually comes pre-installed with Python)

**How to Use**

_Install Dependencies:_
Make sure you have Python installed on your system.
Tkinter is used for the GUI and is usually included with Python.

_Clone the Repository:_
git clone https://github.com/yourusername/random-quotation-generator.git

_Navigate to the Directory:_
cd random-quotation-generator


_Usage:_
Upon running the script, a window will appear with buttons for different categories.
Click on a category button to display a random quotation from that category.

_Contributing:_
Feel free to contribute to this project by opening issues or pull requests.

